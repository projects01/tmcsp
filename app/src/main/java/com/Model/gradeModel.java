package com.Model;

import java.io.Serializable;

public class gradeModel implements Serializable {

    private String subject;
    private String prelim;
    private String midterm;
    private String semiFinal;
    private String finals;
    private String grade;
    private String reex;

    public gradeModel(){}

    public gradeModel(String subject, String prelim, String midterm, String semiFinal, String finals, String grade, String reex){
        this.subject = subject;
        this.prelim = prelim;
        this.midterm = midterm;
        this.semiFinal = semiFinal;
        this.finals = finals;
        this.grade = grade;
        this.reex = reex;
    }

    //setters
    public void setSubject(String subject){ this.subject = subject;}
    public void setPrelim(String prelim){this.prelim = prelim;}
    public void setMidterm(String midterm){this.midterm = midterm;}
    public void setSemiFinal(String semiFinal){this.semiFinal = semiFinal;}
    public void setFinals(String finals){this.finals = finals;}
    public void setGrade(String grade){this.grade = grade;}
    public void setReex(String reex){this.reex = reex;}

    //getters
    public String getSubject(){return subject;}
    public String getPrelim(){return prelim;}
    public String getMidterm(){return midterm;}
    public String getSemiFinal(){return semiFinal;}
    public String getFinals(){return finals;}
    public String getGrade(){return grade;}
    public String getReex(){return reex;}
}
