package com.Model;

import java.io.Serializable;

public class unenrolledModel implements Serializable {

    private String subject;
    private String description;

    public unenrolledModel(){

    }

    //setters
    public void setSubject(String subject){this.subject=subject;}
    public void setDescription(String description){this.description=description;}

    //getters
    public String getSubject(){return subject;}
    public String getDescription(){return description;}


}
