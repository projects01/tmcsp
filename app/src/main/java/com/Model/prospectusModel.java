package com.Model;

public class prospectusModel {

    private String subject;
    private String descriptions;
    private int level;
    private int semester;

    public prospectusModel(){}

    public prospectusModel(String subject, String descriptions, int level, int semester){
        this.subject = subject;
        this.descriptions = descriptions;
        this.level = level;
        this.semester = semester;
    }

    //setters
    public void setSubject(String subject){this.subject=subject;}
    public void setDescriptions(String descriptions){this.descriptions=descriptions;}
    public void setLevel(int level){this.level=level;}
    public void setSemester(int semester){this.semester=semester;}

    //getters
    public String getSubject(){return subject;}
    public String getDescriptions(){return descriptions;}
    public int getLevel(){return level;}
    public int getSemester(){return semester;}
}
