package com.Model;
import java.io.Serializable;

public class loginModel implements Serializable {

    private int curval;
    private int courseid;
    private int id;
    private String student_ID;
    private String password;
    private String date;
    private int sysid;
    private String firstname;
    private String lastname;
    private String middlename;
    private String suffix;
    private String street;
    private String barrangay;
    private String municipality;
    private String city;
    private String birth;
    private int age;
    private String sex;
    private String citizenship;
    private int yearlevel;
    private int status;

    public loginModel(){}

    public loginModel(int curval, int courseid, int id, String student_ID, String password, String date, int sysid, String firstname, String lastname, String middlename, String suffix, String street, String barrangay, String municipality, String city, String birth, int age, String sex, String citizenship, int yearlevel, int status){

        this.curval = curval;
        this.courseid = courseid;
        this.id = id;
        this.sysid = sysid;
        this.student_ID = student_ID;
        this.password = password;
        this.date = date;
        this.firstname = firstname;
        this.lastname = lastname;
        this.middlename = middlename;
        this.suffix = suffix;
        this.street = street;
        this.barrangay = barrangay;
        this.municipality = municipality;
        this.city = city;
        this.birth = birth;
        this.age = age;
        this.sex = sex;
        this.citizenship = citizenship;
        this.yearlevel = yearlevel;
        this.status = status;

    }

    //setters
    public void setCurval(int curval){this.curval=curval;}
    public void setCourseid(int courseid){this.courseid=courseid;}
    public void setId(int id){this.id = id;}
    public void setStudentID(String student_ID){this.student_ID = student_ID;}
    public void setPassword(String password){
        this.password = password;
    }
    public void setDate(String date){this.date = date;}
    public void setSysid(int sysid){this.sysid = sysid; }
    public void setFirstname(String firstname){this.firstname = firstname;}
    public void setLastname(String lastname){this.lastname = lastname;}
    public void setMiddlename(String middlename){this.middlename = middlename;}
    public void setSufix(String suffix){this.suffix = suffix;}
    public void setStreet(String street){this.street = street;}
    public void setBarrangay(String barangay){this.barrangay = barangay;}
    public void setMunicipality(String municipality){this.municipality = municipality;}
    public void setCity(String city){this.city = city;}
    public void setBirth(String birth){this.birth = birth;}
    public void setAge(int age){this.age = age;}
    public void setSex(String sex){this.sex = sex;}
    public void setCitizenship(String citizenship) {this.citizenship = citizenship;}
    public void setYearlevel(int yearlevel){this.yearlevel = yearlevel;}
    public void setStat(int status){this.status = status;}

    //getters
    public int getCurval(){return curval;}
    public int getCourseid(){return courseid;}
    public int getId(){
        return id;
    }
    public String getStudentID(){return student_ID;}
    public String getPassword(){return password;}
    public String getDate(){return date;}
    public int getSysid(){return sysid;}
    public String getFirstname(){return firstname;}
    public String getLastname(){return lastname;}
    public String getMiddlename(){return middlename;}
    public String getSufix(){return suffix;}
    public String getStreet(){return street;}
    public String getBarrangay(){return barrangay;}
    public String getMunicipality(){return municipality;}
    public String getCity(){return city;}
    public String getBirth(){return birth;}
    public int getAge(){return age;}
    public String getSex(){return sex;}
    public String getCitizenship(){return citizenship;}
    public int getYearlevel(){ return yearlevel;}
    public int getStat(){return status;}

}
