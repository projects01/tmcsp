package com.lib;

import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

public class DB {

    private static String address = "http://192.168.43.159/portal/tmcconnector/";
    public DB(){}

    public static HttpURLConnection connect(String path, String method) throws ProtocolException {
        HttpURLConnection con = null;
        try {
            URL url = new URL(address + path);
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod(method);
        }catch(Exception e){
            e.printStackTrace();
        }
            return con;
    }
}
