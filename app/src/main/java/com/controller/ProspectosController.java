package com.controller;

import android.graphics.Color;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.Model.gradeModel;
import com.Model.loginModel;
import com.Model.prospectusModel;
import com.fandriod.tmcstudentportal.R;
import com.lib.Controller;
import com.lib.DB;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;


public class ProspectosController extends Controller {

    ListView lv;
    ArrayAdapter<String> adapter;
    InputStream is = null;
    String line = null;
    String result = null;
    String data[] = null;
    String datas[] = null;
    ArrayList<loginModel> user;
    ArrayList<prospectusModel> prospectus;
    Spinner year;
    Spinner sem;

    public ProspectosController(){

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v =  inflater.inflate(R.layout.prospectos_fragment, container, false);

        prospectus = new ArrayList<prospectusModel>(2);

        StrictMode.setThreadPolicy((new StrictMode.ThreadPolicy.Builder().permitNetwork().build()));

        //Spinners here
        year = v.findViewById(R.id.year);
        String[] items = new String[]{"all", "1st year", "2nd year", "3rd year", "4th year"};
        ArrayAdapter<String> yearAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, items);
        year.setAdapter(yearAdapter);

        //year spinner onItemselectedListener
        year.setOnItemSelectedListener(
            new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

                    Object item = parent.getItemAtPosition(pos);
                    if(!item.toString().equals("all")){
                        String s = sem.getSelectedItem().toString();
                        selectedView(item.toString(), s);
                    }else{
                        selectedView("0", "0");
                    }
                }
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

        //Semestral Spinners here!
        sem = v.findViewById(R.id.sem);
        String[] semestral = new String[]{"wholeyear", "1st semester", "2nd semester", "Summer"};
        ArrayAdapter<String> semetralAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, semestral);
        sem.setAdapter(semetralAdapter);

        //semestral spinner onItemSelectedListener
        //year spinner onItemselectedListener
        sem.setOnItemSelectedListener(
            new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

                    Object item = parent.getItemAtPosition(pos);
                    if(!item.toString().equals("wholeyear")){
                        String y = year.getSelectedItem().toString();
                        selectedView(y, item.toString());
                    }else{
                        selectedView("0", "0");
                    }
                }
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });



        lv = (ListView) v.findViewById(R.id.list);

        displayProspectus(0,0);
//        selectedView("0","0");

//        lv.setAdapter(adapter);

        return v;
    }


    private void selectedView(String year, String sem){

        int y = 0;
        int s = 0;

        if(year.equals("1st year")){
            y = 1;
        }else if(year.equals("2nd year")){
            y = 2;
        }else if(year.equals("3rd year")){
            y = 3;
        }else if(year.equals("4th year")){
            y = 4;
        }else{
            y = 0;
        }

        if(sem.equals("1st semester")){
            s = 1;
        }else if(sem.equals("2nd semester")){
            s = 2;
        }else if(sem.equals("Summer")){
            s = 3;
        }else
        {
            s = 0;
        }

        displayProspectus(y, s);
    }



    private String getProspectus(int y, int s){

        try {
            String year = String.valueOf(y);
            String sem = String.valueOf(s);
            String Sysid = String.valueOf(user.get(0).getSysid()).trim();
            String curval = String.valueOf(user.get(0).getCurval()).trim();
            String courseid = String.valueOf(user.get(0).getCourseid()).trim();
            HttpURLConnection con = DB.connect("student/prospectus/","POST");
            con.setDoOutput(true);
            con.setDoInput(true);
            OutputStream outputStream = con.getOutputStream();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            String post_data =  URLEncoder.encode("Sysid","UTF-8")+"="+URLEncoder.encode(Sysid,"UTF-8")+"&"
                              + URLEncoder.encode("curval","UTF-8")+"="+URLEncoder.encode(curval,"UTF-8")+"&"
                              + URLEncoder.encode("courseid","UTF-8")+"="+URLEncoder.encode(courseid,"UTF-8")+"&"
                              + URLEncoder.encode("year","UTF-8")+"="+URLEncoder.encode(year,"UTF-8")+"&"
                              + URLEncoder.encode("sem","UTF-8")+"="+URLEncoder.encode(sem,"UTF-8");
            bufferedWriter.write(post_data);
            bufferedWriter.flush();
            bufferedWriter.close();
            outputStream.close();

            is = con.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"));

            StringBuilder result = new StringBuilder();
            String line;

            while((line = bufferedReader.readLine())!= null) {
                result.append(line);
            }

            bufferedReader.close();
            is.close();
            con.disconnect();

            return result.toString();

        }catch(Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public void displayProspectus(int year, int s) {

        String subjects = this.getProspectus(year, s);

        //Parse JSON data
        try {
            JSONArray ja = new JSONArray(subjects);
            JSONObject jo = null;

            data = new String[ja.length()];

            int level = 1;
            int sem = 1;
            for (int i = 0; i < ja.length(); i++) {
                jo = ja.getJSONObject(i);
                //data[i] = jo.getString("subject");
                prospectusModel model = new prospectusModel();
                model.setSubject(check(jo.getString("subject")));
                model.setDescriptions(check(jo.getString("descriptions")));
                model.setLevel(Integer.valueOf(check(jo.getString("level"))));
                model.setSemester(Integer.valueOf(check(jo.getString("semester"))));

                data[i] = model.getSubject() + " - " + model.getDescriptions();
                prospectus.add(model);
            }

            Toast.makeText(getContext(), String.valueOf(ja.length()) + " Subjects", Toast.LENGTH_SHORT).show();

            //adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, data);
            lv.setAdapter(null);
            lv.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, data));

        } catch (JSONException e) {
            e.printStackTrace();
        }
//        lv.setAdapter(null);
//        lv.setAdapter(adapter);
//        adapter.notifyDataSetChanged();
    }

    private String year (int year){

        String st = "";
        switch(year){
            case 1 :
                st = "st";
            break;

            case 2 :
                st = "nd";
            break;

            case 3 :
                st = "rd";
                break;

            case 4 :
                st = "th";
                break;
        }

        return st;
    }


//    for (int i = 0; i < ja.length()+4; i++) {
//        jo = ja.getJSONObject(i);
//        //data[i] = jo.getString("subject");
//        prospectusModel model = new prospectusModel();
//        model.setSubject(check(jo.getString("subject")));
//        model.setDescriptions(check(jo.getString("descriptions")));
//        model.setLevel(Integer.valueOf(check(jo.getString("level"))));
//        model.setSemester(Integer.valueOf(check(jo.getString("semester"))));
//
//        if(level == model.getLevel()){
//
//            if(model.getSemester() == sem){
//                //data[i] = sem + "st Semester";
//                //i=i+1;
//                data[i] = model.getSubject() + " - " + model.getDescriptions();
//            }else{
//                data[i] = model.getSubject() + " - " + model.getDescriptions();
//                sem = model.getSemester();
//            }
//        }else{
//            level = model.getLevel();
//            //data[i] = "Level " + level + "st year";
//            //i=i+1;
//            data[i] = model.getSubject() + " - " + model.getDescriptions();
//        }
//
//        data[i] = model.getSubject() + " - " + model.getDescriptions();
//        prospectus.add(model);
//    }

    //    private void getData(){
//
//        try {
//            is = new BufferedInputStream(DB.connect("student/query/","GET").getInputStream());
//        }catch(Exception e){
//            e.printStackTrace();
//        }
//
//        //Read is content into string
//        try{
//            BufferedReader br = new BufferedReader(new InputStreamReader(is));
//            StringBuilder sb = new StringBuilder();
//            while((line = br.readLine()) != null){
//                sb.append(line + "\n");
//            }
//            is.close();
//            result = sb.toString();
//        }catch(Exception e){
//            e.printStackTrace();
//        }
//
//        //Parse JSON data
//        try{
//            JSONArray ja = new JSONArray(result);
//            JSONObject jo = null;
//
//            data = new String[ja.length()];
//
//            for(int i = 0; i < ja.length(); i++){
//                jo = ja.getJSONObject(i);
//                data[i] = jo.getString("username");
//            }
//
//        }catch(Exception e){
//            e.printStackTrace();
//        }
//    }

    private String check(String st){
        if(!st.equals("null")){
            return st;
        }
        return "";
    }

    public void setArguments(ArrayList<loginModel> user) {
        this.user = user;
    }
}
