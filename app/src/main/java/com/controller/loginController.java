package com.controller;

import android.os.AsyncTask;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.fandriod.activities.MainActivity;
import com.fandriod.activities.dashboard;
import com.fandriod.tmcstudentportal.R;
import com.lib.DB;
import com.Model.loginModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

public class loginController extends AsyncTask<String, Void, String>{
    private Context context;
    private AlertDialog alertDialog;
    String data[] = null;

    public loginController (Context ctx) {
        context = ctx;
    }
    @Override
    protected String doInBackground(String... params) {
        String type = params[0];
        if(type.equals("login")) {
            try {
                String username = params[1];
                String password = params[2];

                HttpURLConnection con = DB.connect("student/login/","POST");
                con.setDoOutput(true);
                con.setDoInput(true);
                OutputStream outputStream = con.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                String post_data = URLEncoder.encode("username","UTF-8")+"="+URLEncoder.encode(username,"UTF-8")+"&" + URLEncoder.encode("password","UTF-8")+"="+URLEncoder.encode(password,"UTF-8");
                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = con.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                StringBuilder result = new StringBuilder();
                String line;
                while((line = bufferedReader.readLine())!= null) {
                    result.append(line);
                }
                bufferedReader.close();
                inputStream.close();
                con.disconnect();

                return result.toString();
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

//    https://www.youtube.com/watch?v=zHGgSd1wvxY

    @Override
    protected void onPreExecute() {
        alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle("Login Status");
    }

    @Override
    protected void onPostExecute(String result) {
        try{
            JSONObject object = new JSONObject(result);
            JSONArray credentials  = object.getJSONArray("credentials");
            String status  = object.getString("status").trim();

                if(status.equals("success")){

                    ArrayList<loginModel> Container = new ArrayList<loginModel>(2);

                    for (int i = 0; i < credentials.length(); i++)
                     {
                        JSONObject Jasonobject = credentials.getJSONObject(i);

                        //set model here
                        loginModel model = new loginModel();
                        model.setCurval(Integer.valueOf(Jasonobject.getString("curval").trim()));
                        model.setCourseid(Integer.valueOf(Jasonobject.getString("courseid").trim()));
                        model.setId(Integer.valueOf(Jasonobject.getString("id").trim()));
                        model.setStudentID(Jasonobject.getString("student_id").trim());
                        model.setPassword(Jasonobject.getString("password").trim());
                        model.setDate(Jasonobject.getString("created_at").trim());
                        model.setSysid(Integer.valueOf(Jasonobject.getString("sysid").trim()));
                        model.setFirstname(Jasonobject.getString("firstname").trim());
                        model.setLastname(Jasonobject.getString("lastname").trim());
                        model.setMiddlename(Jasonobject.getString("middlename").trim());
                        model.setSufix(Jasonobject.getString("suffix").trim());
                        model.setStreet(Jasonobject.getString("street").trim());
                        model.setBarrangay(Jasonobject.getString("barrangay").trim());
                        model.setMunicipality(Jasonobject.getString("municipality").trim());
                        model.setCity(Jasonobject.getString("city").trim());
                        model.setBirth(Jasonobject.getString("birth").trim());
                        model.setAge(Integer.valueOf(Jasonobject.getString("age").trim()));
                        model.setSex(Jasonobject.getString("sex").trim());
                        model.setCitizenship(Jasonobject.getString("citizenship").trim());
                        model.setYearlevel(Integer.valueOf(Jasonobject.getString("yearlevel").trim()));
                        model.setStat(Integer.valueOf(Jasonobject.getString("status").trim()));

                         if(!Container.contains(model)){
                         Container.add(model);
                     }
                         //System.out.println("id : " + id + " student ID : " + student_id + " password : " + password + " Date : " + date);
                     }

                    Intent intent = new Intent(context, dashboard.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("user", Container);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                    alertDialog.setMessage("Login Successfully!");
                    MainActivity activity = (MainActivity) context;
                    activity.finish();

                }else{
                    alertDialog.setMessage("Login not Successfull!");
                }

        }catch(Exception e){
            e.printStackTrace();
        }

        alertDialog.show();
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }
}
