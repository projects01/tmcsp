package com.controller;

import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.TextViewCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import com.Model.gradeModel;
import com.Model.loginModel;
import com.lib.Controller;
import com.lib.DB;
import com.fandriod.tmcstudentportal.R;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;

public class GradeController extends Controller implements View.OnClickListener {

    ListView lv;
    ArrayAdapter<String> adapter;
    InputStream is = null;
    String line = null;
    String result = null;
    String data[] = null;

    TextView syStart;
    TextView syEnd;
    Spinner dropdown;
    TableLayout tbllayout;

    private ArrayList<loginModel> user;

    public GradeController(){

    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v =  inflater.inflate(R.layout.grade_fragment, container, false);

        Button go = (Button) v.findViewById(R.id.go);
        go.setOnClickListener(this);

        //database code starts here!
        //Allow network in the main thread
        StrictMode.setThreadPolicy((new StrictMode.ThreadPolicy.Builder().permitNetwork().build()));

        dropdown = v.findViewById(R.id.semester);
        String[] items = new String[]{"all", "1", "2", "Summer", "wholeyear"};
        ArrayAdapter<String> adapt = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, items);
        dropdown.setAdapter(adapt);

        //set default start and end date
        syStart = (TextView) v.findViewById(R.id.syStart);
        syStart.setText(String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));

        syEnd = (TextView) v.findViewById(R.id.syEnd);
        syEnd.setText(String.valueOf(Calendar.getInstance().get(Calendar.YEAR)+1));
        syEnd.setEnabled(false);

        syStart.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s)
                {
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after)
                {
                    /*This method is called to notify you that, within s, the count characters beginning at start are about to be replaced by new text with length after. It is an error to attempt to make changes to s from this callback.*/
                }

                public void onTextChanged(CharSequence s, int start, int before, int count)
                {
                    try {
                        int st = Integer.valueOf(syStart.getText().toString()) + 1;
                        syEnd.setText(String.valueOf(st));
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
            }
        );

        tbllayout = v.findViewById(R.id.table);

        //Retrieve data
        displayGrades(v);

        return v;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.go :
                displayGrades(v);
                break;
        }
    }

   private String getGrade(){

        String semester = "";
       if(dropdown.getSelectedItem() != null){
           semester = dropdown.getSelectedItem().toString();
       }
       String starts = syStart.getText().toString().trim();
       String ends = syEnd.getText().toString().trim();

       try {
           String Sysid = String.valueOf(user.get(0).getSysid()).trim();
           String status = String.valueOf(user.get(0).getStat()).trim();
           HttpURLConnection con = DB.connect("student/grade/","POST");
           con.setDoOutput(true);
           con.setDoInput(true);
           OutputStream outputStream = con.getOutputStream();
           BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
             String post_data =  URLEncoder.encode("Sysid","UTF-8")+"="+URLEncoder.encode(Sysid,"UTF-8")+"&"
                               + URLEncoder.encode("semester","UTF-8")+"="+URLEncoder.encode(semester,"UTF-8")+"&"
                               + URLEncoder.encode("starts","UTF-8")+"="+URLEncoder.encode(starts,"UTF-8")+"&"
                               + URLEncoder.encode("ends","UTF-8")+"="+URLEncoder.encode(ends,"UTF-8")+"&"
                               + URLEncoder.encode("status","UTF-8")+"="+URLEncoder.encode(status,"UTF-8");
           bufferedWriter.write(post_data);
           bufferedWriter.flush();
           bufferedWriter.close();
           outputStream.close();

           is = con.getInputStream();
           BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"));

           StringBuilder result = new StringBuilder();
           String line;

           while((line = bufferedReader.readLine())!= null) {
               result.append(line);
           }

           bufferedReader.close();
           is.close();
           con.disconnect();

           return result.toString();

       }catch(Exception e) {
           e.printStackTrace();
       }

       return null;
   }

   public ArrayList<gradeModel> grades() {

       ArrayList<gradeModel> grademodel = new ArrayList<gradeModel>(2);
       String grades = this.getGrade();

       //Parse JSON data
       try {
           JSONArray ja = new JSONArray(grades);
           JSONObject jo = null;

           data = new String[ja.length()];

           for (int i = 0; i < ja.length(); i++) {
               jo = ja.getJSONObject(i);
               //data[i] = jo.getString("subject");
               gradeModel model = new gradeModel();
                   model.setSubject(check(jo.getString("subject")));
                   model.setPrelim(check(jo.getString("prelim")));
                   model.setMidterm(check(jo.getString("midterm")));
                   model.setSemiFinal(check(jo.getString("semiFinal")));
                   model.setFinals(check(jo.getString("final")));
                   model.setGrade(check(jo.getString("grade")));
                   model.setReex(check(jo.getString("reex")));
               grademodel.add(model);
           }
           return grademodel;

       } catch (JSONException e) {
           e.printStackTrace();
       }
       return null;
   }

   public void displayGrades(View v){
       Toast.makeText(getContext(),  " Grades", Toast.LENGTH_SHORT).show();

       try{
            ArrayList<gradeModel> model = (ArrayList<gradeModel>) this.grades();
           tbllayout.removeAllViewsInLayout();
           tbllayout.removeAllViews();
           tbllayout.setHorizontalGravity(Gravity.CENTER_HORIZONTAL);

           TableRow headrow = new TableRow(getContext());
           TableRow.LayoutParams lps = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
           headrow.setLayoutParams(lps);

           //subject
           TextView s = new TextView(getContext());
           s.setText("Subjects");
           s.setPadding(10, 10, 10, 10);
           TextViewCompat.setAutoSizeTextTypeWithDefaults(s, TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);
           //s.setHeight(100);
           s.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.border));
           s.setGravity(Gravity.LEFT);
           headrow.addView(s);

           //prelim
           TextView p = new TextView(getContext());
           p.setText("Prelim");
           p.setPadding(10, 10, 10, 10);
           TextViewCompat.setAutoSizeTextTypeWithDefaults(p, TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);
           //p.setHeight(100);
           p.setGravity(Gravity.LEFT);
           p.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.border));
           headrow.addView(p);

           //midterm
           TextView m = new TextView(getContext());
           m.setText("Midterm");
           m.setPadding(10, 10, 10, 10);
           TextViewCompat.setAutoSizeTextTypeWithDefaults(m, TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);
           //m.setHeight(100);
           m.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.border));
           m.setGravity(Gravity.LEFT);
           headrow.addView(m);

           //semiFinal
           TextView sf = new TextView(getContext());
           sf.setText("S-Final");
           sf.setPadding(10, 10, 10, 10);
           TextViewCompat.setAutoSizeTextTypeWithDefaults(sf, TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);
           //sf.setHeight(100);
           sf.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.border));
           sf.setGravity(Gravity.LEFT);
           headrow.addView(sf);

           //final
           TextView f = new TextView(getContext());
           f.setText("Final");
           f.setPadding(10, 10, 10, 10);
           TextViewCompat.setAutoSizeTextTypeWithDefaults(f, TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);
           //f.setHeight(100);
           f.setGravity(Gravity.LEFT);
           f.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.border));
           headrow.addView(f);

           //grade
           TextView g = new TextView(getContext());
           g.setText("Grade");
           g.setPadding(10, 10, 10, 10);
           TextViewCompat.setAutoSizeTextTypeWithDefaults(g, TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);
           //g.setHeight(100);
           g.setGravity(Gravity.LEFT);
            g.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.gradecolor));
           headrow.addView(g);

           //reex
           TextView r = new TextView(getContext());
           r.setText("Re-Ex");
           r.setPadding(10, 10, 10, 10);
           TextViewCompat.setAutoSizeTextTypeWithDefaults(r, TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);
           //r.setHeight(100);
           r.setGravity(Gravity.LEFT);
           r.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.border));
           headrow.addView(r);

           tbllayout.addView(headrow,0);



           for(int i = 0; i < model.size(); i++) {

               TableRow row = new TableRow(getContext());
               TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT);
               row.setLayoutParams(lp);

               //subject
               TextView subject = new TextView(getContext());
               subject.setText(model.get(i).getSubject());
               subject.setPadding(10, 10, 10, 10);
               subject.setWidth(250);
               subject.setHeight(120);
               subject.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.border));
               subject.setGravity(Gravity.LEFT);
               row.addView(subject);

               //prelim
               TextView prelim = new TextView(getContext());
               prelim.setText(model.get(i).getPrelim());
               prelim.setPadding(10, 10, 10, 10);
               prelim.setWidth(80);
               prelim.setHeight(120);
               prelim.setGravity(Gravity.LEFT);
               prelim.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.border));
               row.addView(prelim);

               //midterm
               TextView midterm = new TextView(getContext());
               midterm.setText(model.get(i).getMidterm());
               midterm.setPadding(10, 10, 10, 10);
               midterm.setWidth(80);
               midterm.setHeight(120);
               midterm.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.border));
               midterm.setGravity(Gravity.LEFT);
               row.addView(midterm);

               //semiFinal
               TextView semiFinal = new TextView(getContext());
               semiFinal.setText(model.get(i).getSemiFinal());
               semiFinal.setPadding(10, 10, 10, 10);
               semiFinal.setWidth(80);
               semiFinal.setHeight(120);
               semiFinal.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.border));
               semiFinal.setGravity(Gravity.LEFT);
               row.addView(semiFinal);

               //final
               TextView finals = new TextView(getContext());
               finals.setText(model.get(i).getFinals());
               finals.setPadding(10, 10, 10, 10);
               finals.setWidth(80);
               finals.setHeight(120);
               finals.setGravity(Gravity.LEFT);
               finals.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.border));
               row.addView(finals);

               //grade
               TextView grade = new TextView(getContext());
               grade.setText(model.get(i).getGrade());
               grade.setPadding(10, 10, 10, 10);
               grade.setWidth(80);
               grade.setHeight(120);
               grade.setGravity(Gravity.LEFT);
               grade.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.gradecolor));
               row.addView(grade);

               //reex
               TextView reex = new TextView(getContext());
               reex.setText(model.get(i).getReex());
               reex.setPadding(10, 10, 10, 10);
               reex.setWidth(80);
               reex.setHeight(120);
               reex.setGravity(Gravity.LEFT);
               reex.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.border));
               row.addView(reex);

               tbllayout.addView(row, i + 1);
//            data[i] = model.get(i).getSubject();
           }
        }catch(Exception e){
            e.printStackTrace();
        }
   }

   private String check(String st){
        if(!st.equals("null")){
            return st;
        }
        return "";
   }
       //set user model here
    public void setArguments(ArrayList<loginModel> container) {
        this.user = (ArrayList<loginModel>) container;
    }
}
