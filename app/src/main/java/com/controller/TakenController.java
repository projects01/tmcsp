package com.controller;

import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.Model.loginModel;
import com.Model.prospectusModel;
import com.Model.takenModel;
import com.fandriod.tmcstudentportal.R;
import com.lib.Controller;
import com.lib.DB;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

public class TakenController extends Controller {

    private ArrayList<loginModel> user;
    private ArrayList<takenModel> subjects;
    private ListView lv;
    private String data[] = null;
    private InputStream is = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.taken_fragment, container, false);
        lv = v.findViewById(R.id.list);

        subjects = new ArrayList<takenModel>(2);

        StrictMode.setThreadPolicy((new StrictMode.ThreadPolicy.Builder().permitNetwork().build()));
        displayTaken();
        return v;
    }


    private String getTaken(){

        try {
            String Sysid = String.valueOf(user.get(0).getSysid()).trim();
            String curval = String.valueOf(user.get(0).getCurval()).trim();
            String courseid = String.valueOf(user.get(0).getCourseid()).trim();
            String status = String.valueOf(user.get(0).getStat()).trim();

            HttpURLConnection con = DB.connect("student/taken/","POST");
            con.setDoOutput(true);
            con.setDoInput(true);
            OutputStream outputStream = con.getOutputStream();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            String post_data =  URLEncoder.encode("Sysid","UTF-8")+"="+URLEncoder.encode(Sysid,"UTF-8")+"&"
                    + URLEncoder.encode("curval","UTF-8")+"="+URLEncoder.encode(curval,"UTF-8")+"&"
                    + URLEncoder.encode("courseid","UTF-8")+"="+URLEncoder.encode(courseid,"UTF-8")+"&"
                    + URLEncoder.encode("status","UTF-8")+"="+URLEncoder.encode(status,"UTF-8");
            bufferedWriter.write(post_data);
            bufferedWriter.flush();
            bufferedWriter.close();
            outputStream.close();

            is = con.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"));

            StringBuilder result = new StringBuilder();
            String line;

            while((line = bufferedReader.readLine())!= null) {
                result.append(line);
            }

            bufferedReader.close();
            is.close();
            con.disconnect();

            return result.toString();

        }catch(Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public void displayTaken() {

        String takenSubjects = this.getTaken();

        //Parse JSON data
        try {
            JSONArray ja = new JSONArray(takenSubjects);
            JSONObject jo = null;
            data = new String[ja.length()];
            int level = 1;
            int sem = 1;
            for (int i = 0; i < ja.length(); i++) {
                jo = ja.getJSONObject(i);

                takenModel model = new takenModel();
                model.setSubject(check(jo.getString("subject")));
                model.setDescription(check(jo.getString("description")));
                model.setGrade(check(jo.getString("grade")));
                model.setReex(check(jo.getString("reex")));

                data[i] = model.getSubject() + " - " + model.getDescription();
                subjects.add(model);
            }

            Toast.makeText(getContext(), String.valueOf(ja.length()) + " Subjects", Toast.LENGTH_SHORT).show();
            lv.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, data));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String check(String st){
        if(!st.equals("null")){
            return st;
        }
        return "";
    }


    public void setArguments(ArrayList<loginModel> container) {
        this.user = container;
    }
}
