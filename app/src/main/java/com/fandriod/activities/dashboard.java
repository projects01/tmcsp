package com.fandriod.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.Model.loginModel;
import com.controller.GradeController;
import com.controller.ProspectosController;
import com.controller.TakenController;
import com.controller.UnenrolledController;
import com.fandriod.tmcstudentportal.R;

import java.util.ArrayList;

public class dashboard extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private DrawerLayout drawer;
    private ArrayList<loginModel> Container;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setContentView(R.layout.activity_dashboard);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//
//        Intent i = this.getIntent();
//        Bundle bundle = i.getExtras();
//        loginModel user = (loginModel) bundle.getSerializable("user");

        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        Container = (ArrayList<loginModel>)bundle.getSerializable("user");

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);
        TextView profile = (TextView) headerView.findViewById(R.id.profile);
        profile.setText(Container.get(0).getFirstname() + " " + Container.get(0).getMiddlename() + " " + Container.get(0).getLastname());

        ActionBarDrawerToggle toogle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toogle);
        toogle.syncState();


        //default fragment
        if(savedInstanceState == null) {
            GradeController grade = new GradeController();
            grade.setArguments(Container);
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, grade).commit();
            navigationView.setCheckedItem(R.id.nav_grade);
        }
    }

//    https://stackoverflow.com/questions/40561267/how-to-pass-array-list-as-an-argument-to-the-fragment-and-retrieve-it?rq=1

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch(menuItem.getItemId()){
            case R.id.nav_grade:
                GradeController grade = new GradeController();
                grade.setArguments(Container);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, grade).commit();
                break;
            case R.id.nav_taken:
                TakenController taken = new TakenController();
                taken.setArguments(Container);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, taken ).commit();
                break;
            case R.id.nav_unenrolled:
                UnenrolledController unenrolled = new UnenrolledController();
                unenrolled.setArguments(Container);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, unenrolled ).commit();
                break;
            case R.id.nav_prospectus:
                ProspectosController prospectus = new ProspectosController();
                prospectus.setArguments(Container);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, prospectus ).commit();
                break;
            case R.id.nav_logout:
                Intent intent = new Intent(this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                Toast.makeText(this, "Logout", Toast.LENGTH_SHORT).show();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if(drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        }else{
            super.onBackPressed();
        }
        super.onBackPressed();
    }
}
