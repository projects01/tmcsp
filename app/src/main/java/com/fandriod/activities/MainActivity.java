package com.fandriod.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import com.controller.loginController;
import com.fandriod.tmcstudentportal.R;

public class MainActivity extends AppCompatActivity
            implements View.OnClickListener{

    private EditText username;
    private EditText password;
    private Button login;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        login = (Button) findViewById(R.id.btnLogin);

        login.setOnClickListener(this);

//        login.setOnClickListener(new View.OnClickListener() {
//        });
    }

    private void validate(String username, String password){
        String type = "login";
        loginController backgroundWorker = new loginController(this);
        backgroundWorker.execute(type, username, password);
    }

    @Override
    public void onClick(View v) {
        validate(username.getText().toString(), password.getText().toString());

    }

}
